Dein Junior-Kollege bittet dich ein Code Review für das folgende Ticket durchzuführen:

`Wir wollen unserem Kunden von my-fancy-shop.com einen URL Shortener zur Verfügung stellen. Dabei sollen möglichst wenig alphanumerische Zeichen in die URL mit einfließen. Es ist wichtig, dass short Urls langfristig aufgelöst werden können, da diese über social Media geteilt werden.
`

Da wir Common Code Ownership leben, refactore den Code gerne , so dass er die Aufgabe erfüllt.

Mache gerne Verbesserungen die dir sinnvoll erscheinen, so dass der shortener in Produktion verwendet werden kann.

