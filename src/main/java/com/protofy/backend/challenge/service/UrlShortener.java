package com.protofy.backend.challenge.service;

import java.util.HashMap;
import java.util.Random;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

@Service
public class UrlShortener {
    HashMap urls = new HashMap();

    public String storeUrl(String url) {
        String shortUrl = "";
        do {
            shortUrl = RandomStringUtils.randomAlphanumeric(5);
        } while (urls.containsKey(shortUrl));

        urls.put(shortUrl, url);
        return shortUrl;
    }

    public String resolve(String slug) {
        return (String) urls.get(slug);
    }
}