package com.protofy.backend.challenge.repository;

import com.protofy.backend.challenge.domain.User;
import java.util.List;
import org.springframework.data.repository.CrudRepository;


public interface UserRepository extends CrudRepository<User, Long> {

    List<User> findByLastName(String lastName);

}
